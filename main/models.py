from django.db import models
from django.utils import timezone


class User(models.Model):
    """
    User model for storing information about registered users

    Attributes
    -----
    email : str
        User's email address
    registration_ip : char*[15]
        User's ip used to register on network
    mac_adddress: char*[17]
        Mac address of device user is trying to register
    time_updated: datetime
        Last time the user updated their entry

    Notes
    -----
    This is mostly unused, and should not be referenced.

    """

    email = models.EmailField(unique=True)
    registration_ip = models.CharField(max_length=15)
    mac_address = models.CharField(max_length=17)
    time_updated = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.email