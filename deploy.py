import os
import sys
import subprocess

from config.cfg import Cfg


class Colors:
    Header = '\033[95m'
    OkBlue = '\033[94m'
    OkGreen = '\033[92m'
    Warning = '\033[93m'
    Fail = '\033[91m'
    Endc = '\033[0m'
    Bold = '\033[1m'
    Underline = '\033[4m'


BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def main():
    if 'linux' not in sys.platform:
        raise Exception("This code runs on Linux only.")

    print(Colors.Warning + "Be sure to run this script in a virtualenv" + Colors.Endc)

    # Git pull
    print(Colors.OkBlue + "Checking for git updates..." + Colors.Endc)
    result = subprocess.run(["git", "pull"], capture_output=True)
    if result.stderr.decode('utf-8'):
        raise Exception(result.stderr.decode('utf-8'))
    print(result.stdout.decode('utf-8'), "\n")

    # Ensure secret configs exist
    print(Colors.OkBlue + "Checking for secret config files..." + Colors.Endc)
    secret_files = ["sk_django",
                    "clearpass.json",
                    "encryption.json",
                    "administrators.json"]

    for secret_file in secret_files:
        if not os.path.isfile(os.path.join(BASE_DIR, "config/secret/" + secret_file)):
            raise IOError(os.path.join(BASE_DIR, "config/secret/" + secret_file) + " not found")

    # Check if log location is writeable
    print(Colors.OkBlue + "Checking if log is writable..." + Colors.Endc)
    if not os.access(Cfg.LOG_LOCATION, os.W_OK | os.X_OK):
        raise Exception("Log location is not writable. Edit in config/cfg.py")

    # Check for ENV variable for debug env
    if os.getenv("DJANGO_DEBUG") == '1':
        print(Colors.Warning + "IS_DEBUG = TRUE" + Colors.Endc)
    else:
        print(Colors.OkBlue + "IS_DEBUG = FALSE" + Colors.Endc)
        print(Colors.Warning + "To set debug: DJANGO_DEBUG=1; export DJANGO_DEBUG;" + Colors.Endc)

    # Django Migrations
    print(Colors.OkBlue + "Performing Django migrations..." + Colors.Endc)
    result = subprocess.run(["python3", os.path.join(BASE_DIR, "manage.py"), "makemigrations"], capture_output=True)
    print(result.stdout.decode('utf-8'), "\n")
    result = subprocess.run(["python3", os.path.join(BASE_DIR, "manage.py"), "migrate"], capture_output=True)
    print(result.stdout.decode('utf-8'), "\n")

    # Django Static Files
    print(Colors.OkBlue + "Collecting static files..." + Colors.Endc)
    result = subprocess.run(["python3", os.path.join(BASE_DIR, "manage.py"), "collectstatic"], capture_output=True)
    print(result.stdout.decode('utf-8'), "\n")

    # Django deploy check
    print(Colors.OkBlue + "Performing Django deploy check..." + Colors.Endc)
    result = subprocess.run(["python3", os.path.join(BASE_DIR, "manage.py"), "check", "--deploy"], capture_output=True)
    print(result.stdout.decode('utf-8'), "\n")
    if result.stderr.decode('utf-8'):
        print(result.stderr.decode('utf-8'))
        yn = str(input("\n\nDo you wish to continue? [y/n]: ")).lower().strip()
        if yn[0] != 'y':
            print("Exiting...")
            exit()

    # Restart gunicorn

    # Restart nginx


if __name__ == '__main__':
    print(Colors.OkBlue + "Automatedmac Deploy and Update Script" + Colors.Endc)
    print("Note: This repository is meant for syncing with deployment servers.\n"
          "It is highly recommended you do not use this code elsewhere, however,\n"
          "You may do so at your own discretion.\n\n")
    main()
