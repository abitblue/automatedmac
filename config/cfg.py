import os
import json
import yaml
import logging
import utils.connectivity
import pandas as pd

logger = logging.getLogger('Config')


class Cfg:
    CFG_BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    LOG_LOCATION = os.path.join(os.path.dirname(CFG_BASE_DIR), 'logs')

    # Administrators
    administrators = {}
    totp_keys = {}
    totp_settings = {}

    # Clearpass
    clearpass = {}

    # Encryption
    encryption = {}

    # Devices
    devices = {
        'Android_Platforms': [],
        'Allowed_Platforms': [],
        'Allowed_Devices': [],
        'Blocked_Platforms': [],
        'Blocked_Devices': []
    }

    @classmethod
    def init(cls, minimal: bool = False):
        # Minimal setup
        if not os.path.exists(Cfg.LOG_LOCATION):
            os.makedirs(Cfg.LOG_LOCATION)

        # Minimal and nonminmal setup
        if not minimal:
            logger.info("Reading configuration files...")
            try:
                # Administrators
                with open(os.path.join(Cfg.CFG_BASE_DIR, 'secret/administrators.json')) as f:
                    conf = json.loads(f.read())
                    cls.administrators = conf['Administrators'].copy()
                    cls.totp_keys = conf['TotpKeys'].copy()
                    cls.totp_settings = conf['TotpSettings'].copy()
                assert len(cls.administrators) != 0, "There must be at least 1 administrator"
                assert cls.totp_settings['AdminIDLength'] <= 16, "There must be at most 16 characters to an ID"
                assert cls.totp_settings['TOTPLength'] <= 8, "There must be at most 8 characters to an TOTP"

                # Clearpass
                with open(os.path.join(Cfg.CFG_BASE_DIR, 'secret/clearpass.json')) as f:
                    conf = json.loads(f.read())
                    cls.clearpass = conf.copy()
                assert list(cls.clearpass.keys()) == ['ClientID', 'SecretKey', 'APIEndPoint'], "Bad Clearpass config"

                # Encryption
                with open(os.path.join(Cfg.CFG_BASE_DIR, 'secret/encryption.json')) as f:
                    conf = json.loads(f.read())
                    cls.encryption = conf.copy()
                assert list(cls.encryption.keys()) == ['X25519-Private', 'X25519-Public', 'Ed25519-Sign',
                                                       'Ed25519-Verify'], "Bad Encryption config"

                # Devices
                if utils.connectivity.has_internet():
                    url = "http://storage.googleapis.com/play_public/supported_devices.csv"
                    logger.info('Checking for update for devices list...')
                    if utils.connectivity.file_update(os.path.join(Cfg.CFG_BASE_DIR, 'devices.csv'), url):
                        logger.info('Devices list updated')
                    else:
                        logger.info('No devices list update found')
                else:
                    logger.warning('Cannot connect to internet')
                    assert os.path.isfile(os.path.join(Cfg.CFG_BASE_DIR, 'devices.csv')),\
                        "Cannot locate config/devices.csv"

                assert os.path.isfile(os.path.join(Cfg.CFG_BASE_DIR, 'devices.yaml')),\
                    "Cannot locate config/devices.yaml"

                logger.info('Parsing devices list')

                with open(os.path.join(Cfg.CFG_BASE_DIR, 'devices.yaml'), 'r') as f:
                    conf = yaml.safe_load(f)

                    cls.devices['Android_Platforms'] = conf['Whitelist']['Android'].copy()
                    cls.devices['Allowed_Platforms'] = conf['Whitelist']['Platforms'].copy()
                    cls.devices['Blocked_Platforms'] = conf['Blacklist']['Platforms'].copy()

                    for item in conf['Whitelist']['Devices_Custom']:
                        if item != 'N/A':
                            cls.devices['Allowed_Devices'].append(item)

                    for item in conf['Blacklist']['Devices_Custom']:
                        if item != 'N/A':
                            cls.devices['Blocked_Devices'].append(item)

                    csv = pd.read_csv(os.path.join(Cfg.CFG_BASE_DIR, 'devices.csv'), encoding='utf-16')

                    for brand in conf['Whitelist']['Devices']:
                        for name in brand:
                            brand_filter = csv.loc[csv['Retail Branding'] == name]
                            for regex in brand[name]:
                                device_filter = brand_filter[brand_filter['Marketing Name'].str.contains(regex,
                                                                                                         na=False)]
                                cls.devices['Allowed_Devices'].extend(device_filter['Model'].tolist())

                    for brand in conf['Blacklist']['Devices']:
                        for name in brand:
                            brand_filter = csv.loc[csv['Retail Branding'] == name]
                            for regex in brand[name]:
                                device_filter = brand_filter[brand_filter['Marketing Name'].str.contains(regex,
                                                                                                         na=False)]
                                cls.devices['Blocked_Devices'].extend(device_filter['Model'].tolist())

            except Exception as e:
                print(repr(e))
                exit()

            logger.info("Configuration done")
